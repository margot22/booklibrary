import unittest
import json
from app import create_app
from app.init_db import db


class BookTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app(config_name="testing")
        self.client = self.app.test_client
        self.book = {'title': 'Harry Potter', 'author': 'J.K. Rowling'}

        with self.app.app_context():
            db.create_all()

    def test_book_add(self):
        res = self.client().post('/books', data=self.book)
        self.assertEqual(res.status_code, 201)
        self.assertIn('Harry Potter', str(res.data))

    def test_api_can_get_all_books(self):
        res = self.client().post('/books', data=self.book)
        self.client().post('/books', data={'title': 'a', 'author': 'h'})
        self.client().post('/books', data={'title': 's', 'author': 'h'})
        self.client().post('/books', data={'title': 'v', 'author': 'm'})
        self.client().post('/books', data={'title': 'd', 'author': 'J'})

        self.assertEqual(res.status_code, 201)
        res = self.client().get('/books')
        self.assertEqual(res.status_code, 200)
        self.assertIn('Harry Potter', str(res.data))

    def test_api_can_get_all_authors(self):
        res = self.client().post('/books', data=self.book)
        self.assertEqual(res.status_code, 201)
        res = self.client().get('/authors')
        self.assertEqual(res.status_code, 200)
        self.assertIn('Rowling', str(res.data))

    def test_api_can_get_books_by_id(self):
        rv = self.client().post('/books', data=self.book)
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'", "\""))
        result = self.client().get(
            '/books?id={}'.format(result_in_json['id']))
        self.assertEqual(result.status_code, 200)
        self.assertIn('Harry Potter', str(result.data))

    def test_books_can_be_edited(self):
        rv = self.client().post(
            '/books',
            data={'title': 'Harry Potter', 'author': 'J.K. Rowling'})
        self.assertEqual(rv.status_code, 201)
        rv = self.client().put(
            '/books?id=1',
            data={
                "title": "Harry Potter 2"
            })
        self.assertEqual(rv.status_code, 200)
        results = self.client().get('/books?id={}'.format(1))
        self.assertIn('Harry Potter 2', str(results.data))

    def test_books_deletion(self):
        rv = self.client().post(
            '/books',
            data={'title': 'Harry Potter', 'author': 'J.K. Rowling'})
        self.assertEqual(rv.status_code, 201)
        res = self.client().delete('/books?id=1')
        self.assertEqual(res.status_code, 200)
        result = self.client().get('/books?id=1')
        self.assertEqual(result.status_code, 404)

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()


if __name__ == "__main__":
    unittest.main()
