from flask_sqlalchemy import SQLAlchemy
from flask import request, jsonify, abort, render_template

db = SQLAlchemy()
not_found_error = 'Book not found'
book_already_exists = 'Book already exists'
author_already_exists = 'Author already exists'
provide_author = 'Provide at least one author of the book'


def create_db(app):
    from app.models import Book, Author
    db.init_app(app)

    @app.route('/books', methods=['POST'])
    def add_new_book():
        title = str(request.data.get('title', ''))
        author_name = str(request.data.get('author', ''))
        if title:
            if not author_name:
                abort(400, provide_author)

            authors = Author.get_all()
            if any(obj.name == author_name for obj in authors):
                author = Author.query.filter_by(name=author_name).first()
            else:
                author = Author(name=author_name)
                author.save()

            book = Book(title=title)
            books = Book.get_all()
            if any(obj.title == title for obj in books):
                abort(400, book_already_exists)
            book.authors.append(author)
            book.save()

            response = jsonify(book_object_as_dict(book))
            response.status_code = 201
            return response

        books = Book.get_all()
        results = []

        for book in books:
            obj = book_object_as_dict(book)
            results.append(obj)
        response = jsonify(results)
        response.status_code = 200
        return response

    @app.route('/books', methods=['DELETE'])
    def delete_book(**kwargs):
        id = request.args.get('id', '')
        book = Book.query.filter_by(id=id).first()
        if not book:
            abort(404, not_found_error)
        if request.method == 'DELETE':
            book.delete()
            return {
                       "message": "Book '{}' deleted successfully".format(book.title)
                   }, 200

    @app.route('/books', methods=['GET'])
    def get_book(**kwargs):
        id = request.args.get('id', '')
        if not id:
            books = Book.get_all()
            results = []

            for book in books:
                obj = book_object_as_dict(book)
                results.append(obj)
            response = jsonify(results)
            response.status_code = 200
            return response
        else:
            book = Book.query.filter_by(id=id).first()
            if not book:
                abort(404, not_found_error)
            response = jsonify(book_object_as_dict(book))
            response.status_code = 200
            return response

    @app.route('/books/<int:page_num>', methods=['GET'])
    def get_all_books(page_num, **kwargs):
        response = Book.query.paginate(per_page=10, page=page_num, error_out=True)
        return render_template('index.html', elements=response)

    @app.route('/authors/<int:page_num>', methods=['GET'])
    def get_all_authors(page_num, **kwargs):
        response = Author.query.paginate(per_page=10, page=page_num, error_out=True)
        return render_template('index.html', elements=response)

    @app.route('/books', methods=['PUT'])
    def edit_book(**kwargs):
        id = request.args.get('id', '')
        book = Book.query.filter_by(id=id).first()
        if not book:
            abort(404, not_found_error)

        title = str(request.data.get('title', ''))
        author = str(request.data.get('author', ''))
        books = Book.query.filter(Book.id != book.id).all()

        if any(obj.title == title for obj in books):
            abort(400, book_already_exists)

        book.title = title if title else book.title

        book.save()
        response = jsonify(book_object_as_dict(book))
        response.status_code = 200
        return response

    @app.route('/authors', methods=['GET'])
    def get_author(**kwargs):
        id = request.args.get('id', '')
        if not id:
            authors = Author.get_all()
            results = []

            for author in authors:
                obj = {
                    'id': author.id,
                    'name': author.name
                }
                results.append(obj)
            response = jsonify(results)
            response.status_code = 200
            return response
        else:
            author = Author.query.filter_by(id=id).first()
            if not author:
                abort(404, not_found_error)
            response = jsonify({
                'id': author.id,
                'name': author.name
            })
            response.status_code = 200
            return response

    def book_object_as_dict(book):
        return {
            'id': book.id,
            'title': book.title,
            'rating': book.rating,
            'no_of_votes': book.no_of_votes
        }

    return app

