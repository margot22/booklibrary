from app.init_db import db

authors_books = db.Table('authors_books',
                         db.Column('book_id', db.Integer, db.ForeignKey('book.id'), primary_key=True),
                         db.Column('author_id', db.Integer, db.ForeignKey('author.id'), primary_key=True)
                         )


class Book(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    title = db.Column(db.String(80), unique=True, nullable=False)
    rating = db.Column(db.Integer, unique=False, nullable=True)
    no_of_votes = db.Column(db.Integer, unique=False, nullable=False)
    authors = db.relationship('Author', secondary=authors_books, lazy='subquery',
                              backref=db.backref('pages', lazy=True))

    def __init__(self, title):
        self.title = title
        self.rating = None
        self.no_of_votes = 0

    def save(self):
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_all():
        return Book.query.all()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def __repr__(self):
        return "<Book title: {}>".format(self.title)


class Author(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)

    def __init__(self, name):
        self.name = name

    def save(self):
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_all():
        return Author.query.all()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def __repr__(self):
        return "<Author name: {}>".format(self.name)
